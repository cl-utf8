(in-package "UTF-8")

(defmacro while (test &body body)
  `(do ()
       ((not ,test))
     ,@body))

(defmacro code-point-1-p (a)
  `(= (ldb (byte 1 7) ,a) #b0))

(defmacro code-point-2-p (a b)
  `(and (= (ldb (byte 3 5) ,a) #b110)
        (= (ldb (byte 2 6) ,b) #b10)))

(defmacro code-point-3-p (a b c)
  `(and (= (ldb (byte 4 4) ,a) #b1110)
        (= (ldb (byte 2 6) ,b) #b10)
        (= (ldb (byte 2 6) ,c) #b10)))

(defmacro code-point-4-p (a b c d)
  `(and (= (ldb (byte 5 3) ,a) #b11110)
        (= (ldb (byte 2 6) ,b) #b10)
        (= (ldb (byte 2 6) ,c) #b10)
        (= (ldb (byte 2 6) ,d) #b10)))

(defmacro make-code-point-1 (a)
  a)

(defmacro make-code-point-2 (a b)
  `(logior (ash (ldb (byte 5 0) ,a) 6)
           (ldb (byte 6 0) ,b)))

(defmacro make-code-point-3 (a b c)
  `(logior (ash (ldb (byte 4 0) ,a) (+ 6 6))
           (ash (ldb (byte 6 0) ,b) 6)
           (ldb (byte 6 0) ,c)))

(defmacro make-code-point-4 (a b c d)
  `(logior (ash (ldb (byte 3 0) ,a) (+ 6 6 6))
           (ash (ldb (byte 6 0) ,b) (+ 6 6))
           (ash (ldb (byte 6 0) ,c) 6)
           (ldb (byte 6 0) ,d)))

(define-condition decode-error (error)
  ((seq :reader decode-error-seq :initarg :seq :initform (error ":seq required") :type sequence)
   (pos :reader decode-error-pos :initarg :pos :initform (error ":pos required") :type 'unsigned-byte))
  (:report (lambda (condition stream)
             (format stream "UTF-8 decode error at position ~D" (decode-error-pos condition)))))

(defgeneric decode-sequence (seq &key start end))

(defmethod decode-sequence ((seq vector) &key (start 0) end)
  (unless end
    (setf end (length seq)))
  (assert (<= 0 start end (length seq)))
  (loop for pos from start to (1- end)
        do (assert (<= #x00 (aref seq pos) #xFF)))

  (let ((code-point-list ())
        (pos start))
    (while (< (+ pos 3) end)
      (multiple-value-bind (code-point inc)
          (decode-code-point-4 (aref seq pos) (aref seq (1+ pos)) (aref seq (+ pos 2)) (aref seq (+ pos 3)))
        (unless code-point
          (restart-case
              (error 'decode-error :seq seq :pos pos)
            (use-U+FFFD ()
              :report "Substitute with U+FFFD and continue"
              (setf code-point #xFFFD)
              (setf inc (decode-error-width seq pos end)))))
        (push code-point code-point-list)
        (incf pos inc)))

    (while (< (+ pos 2) end)
      (multiple-value-bind (code-point inc)
          (decode-code-point-3 (aref seq pos) (aref seq (1+ pos)) (aref seq (+ pos 2)))
        (unless code-point
          (restart-case
              (error 'decode-error :seq seq :pos pos)
            (use-U+FFFD ()
              :report "Substitute with U+FFFD and continue"
              (setf code-point #xFFFD)
              (setf inc (decode-error-width seq pos end)))))
        (push code-point code-point-list)
        (incf pos inc)))

    (while (< (1+ pos) end)
      (multiple-value-bind (code-point inc)
          (decode-code-point-2 (aref seq pos) (aref seq (1+ pos)))
        (unless code-point
          (restart-case
              (error 'decode-error :seq seq :pos pos)
            (use-U+FFFD ()
              :report "Substitute with U+FFFD and continue"
              (setf code-point #xFFFD)
              (setf inc (decode-error-width seq pos end)))))
        (push code-point code-point-list)
        (incf pos inc)))

    (while (< pos end)
      (multiple-value-bind (code-point inc)
          (decode-code-point-1 (aref seq pos))
        (unless code-point
          (restart-case
              (error 'decode-error :seq seq :pos pos)
            (use-U+FFFD ()
              :report "Substitute with U+FFFD and continue"
              (setf code-point #xFFFD)
              (setf inc (decode-error-width seq pos end)))))
        (push code-point code-point-list)
        (incf pos inc)))

    (coerce (nreverse code-point-list) 'vector)))

(defun decode-code-point-1 (a)
  (declare (type (unsigned-byte 8) a))
  (cond ((code-point-1-p a)
         (values (make-code-point-1 a) 1))))

(defun decode-code-point-2 (a b)
  (declare (type (unsigned-byte 8) a))
  (declare (type (unsigned-byte 8) b))
  (cond ((code-point-1-p a)
         (values (make-code-point-1 a) 1))

        ((code-point-2-p a b)
         (let ((code-point (make-code-point-2 a b)))
           (if (<= #x0080 code-point)
               (values code-point 2)
               nil)))))

(defun decode-code-point-3 (a b c)
  (declare (type (unsigned-byte 8) a))
  (declare (type (unsigned-byte 8) b))
  (declare (type (unsigned-byte 8) c))
  (cond ((code-point-1-p a)
         (values (make-code-point-1 a) 1))

        ((code-point-2-p a b)
         (let ((code-point (make-code-point-2 a b)))
           (if (<= #x0080 code-point)
               (values code-point 2)
               nil)))

        ((code-point-3-p a b c)
         (let ((code-point (make-code-point-3 a b c)))
           (if (or (<= #x0800 code-point #xD7FF)
                   (<= #xE000 code-point))
               (values code-point 3)
               nil)))))

(defun decode-code-point-4 (a b c d)
  (declare (type (unsigned-byte 8) a))
  (declare (type (unsigned-byte 8) b))
  (declare (type (unsigned-byte 8) c))
  (declare (type (unsigned-byte 8) d))
  (cond ((code-point-1-p a)
         (values (make-code-point-1 a) 1))

        ((code-point-2-p a b)
         (let ((code-point (make-code-point-2 a b)))
           (if (<= #x0080 code-point)
               (values code-point 2)
               nil)))

        ((code-point-3-p a b c)
         (let ((code-point (make-code-point-3 a b c)))
           (if (or (<= #x0800 code-point #xD7FF)
                   (<= #xE000 code-point))
               (values code-point 3)
               nil)))

        ((code-point-4-p a b c d)
         (let ((code-point (make-code-point-4 a b c d)))
           (if (<= #x10000 code-point #x10FFFF)
               (values code-point 4)
               nil)))))

(defun decode-error-width (seq pos end)
  (let ((a (aref seq pos))
        (b (if (< (1+ pos) end)
               (aref seq (1+ pos))
               nil))
        (c (if (< (+ pos 2) end)
               (aref seq (+ pos 2))
               nil)))
    (cond ((and (= a #xE0) b (<= #xA0 b #xBF)) 2)
          ((and (<= #xE1 a #xEC) b (<= #x80 b #xBF)) 2)
          ((and (= a #xED) b (<= #x80 b #x9F)) 2)
          ((and (<= #xEE a #xEF) b (<= #x80 b #xBF)) 2)
          ((and (= a #xF0) b (<= #x90 b #xBF) c (<= #x80 c #xBF)) 3)
          ((and (= a #xF0) b (<= #x90 b #xBF)) 2)
          ((and (<= #xF1 a #xF3) b (<= #x80 b #xBF) c (<= #x80 c #xBF)) 3)
          ((and (<= #xF1 a #xF3) b (<= #x80 b #xBF)) 2)
          ((and (= a #xF4) b (<= #x80 b #x8F) c (<= #x80 c #xBF)) 3)
          ((and (= a #xF4) b (<= #x80 b #x8F)) 2)
          (t 1))))
