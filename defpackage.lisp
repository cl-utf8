(defpackage "UTF-8"
  (:use "CL")
  (:export "DECODE-SEQUENCE"
           "ENCODE-SEQUENCE"
           "DECODE-ERROR"
           "ENCODE-ERROR"))
