(in-package "UTF-8")

(define-condition encode-error (error)
  ((seq :reader encode-error-seq :initarg :seq :initform (error ":seq required") :type sequence)
   (pos :reader encode-error-pos :initarg :pos :initform (error ":pos required") :type 'unsigned-byte))
  (:report (lambda (condition stream)
             (format stream "UTF-8 encode error at position ~D" (encode-error-pos condition)))))

(defgeneric encode-sequence (seq &key start end byte-order-mark))

(defmethod encode-sequence ((seq vector) &key (start 0) end byte-order-mark)
  (unless end
    (setf end (length seq)))
  (assert (<= 0 start end (length seq)))

  (let ((octet-vector (make-array (+ (* 4 (- end start)) (if byte-order-mark
                                                             3
                                                             0))
                                  :element-type '(unsigned-byte 8)))
        (octet-pos 0))
    (when byte-order-mark
      (setf (aref octet-vector 0) #xEF)
      (setf (aref octet-vector 1) #xBB)
      (setf (aref octet-vector 2) #xBF)
      (incf octet-pos 3))
    (loop for pos from start to (1- end)
          for code-point = (aref seq pos)
          do (cond ((<= #x0000 code-point #x007F)
                    (setf (aref octet-vector octet-pos) code-point)
                    (incf octet-pos))

                   ((<= #x0080 code-point #x07FF)
                    (setf (aref octet-vector octet-pos) (logior #b11000000 (ldb (byte 5 6) code-point)))
                    (setf (aref octet-vector (1+ octet-pos)) (logior #b10000000 (ldb (byte 6 0) code-point)))
                    (incf octet-pos 2))

                   ((or (<= #x0800 code-point #xD7FF)
                        (<= #xE000 code-point #xFFFF))
                    (setf (aref octet-vector octet-pos) (logior #b11100000 (ldb (byte 4 12) code-point)))
                    (setf (aref octet-vector (1+ octet-pos)) (logior #b10000000 (ldb (byte 6 6) code-point)))
                    (setf (aref octet-vector (+ octet-pos 2)) (logior #b10000000 (ldb (byte 6 0) code-point)))
                    (incf octet-pos 3))

                   ((<= #x10000 code-point #x10FFFF)
                    (setf (aref octet-vector octet-pos) (logior #b11110000 (ldb (byte 3 18) code-point)))
                    (setf (aref octet-vector (1+ octet-pos)) (logior #b10000000 (ldb (byte 6 12) code-point)))
                    (setf (aref octet-vector (+ octet-pos 2)) (logior #b10000000 (ldb (byte 6 6) code-point)))
                    (setf (aref octet-vector (+ octet-pos 3)) (logior #b10000000 (ldb (byte 6 0) code-point)))
                    (incf octet-pos 4))

                   (t (error 'encode-error :seq seq :pos pos))))
    (adjust-array octet-vector octet-pos)))

(defmethod encode-sequence ((seq list) &key (start 0) end byte-order-mark)
  (unless end
    (setf end (length seq)))
  (assert (<= 0 start end (length seq)))

  (let ((octet-list (if byte-order-mark
                        (nreverse (list #xEF #xBB #xBF))
                        ())))
    (loop for pos from start to (1- end)
          for code-point in (nthcdr start seq)
          do (cond ((<= #x0000 code-point #x007F)
                    (push code-point octet-list))

                   ((<= #x0080 code-point #x07FF)
                    (push (logior #b11000000 (ldb (byte 5 6) code-point)) octet-list)
                    (push (logior #b10000000 (ldb (byte 6 0) code-point)) octet-list))

                   ((or (<= #x0800 code-point #xD7FF)
                        (<= #xE000 code-point #xFFFF))
                    (push (logior #b11100000 (ldb (byte 4 12) code-point)) octet-list)
                    (push (logior #b10000000 (ldb (byte 6 6) code-point)) octet-list)
                    (push (logior #b10000000 (ldb (byte 6 0) code-point)) octet-list))

                   ((<= #x10000 code-point #x10FFFF)
                    (push (logior #b11110000 (ldb (byte 3 18) code-point)) octet-list)
                    (push (logior #b10000000 (ldb (byte 6 12) code-point)) octet-list)
                    (push (logior #b10000000 (ldb (byte 6 6) code-point)) octet-list)
                    (push (logior #b10000000 (ldb (byte 6 0) code-point)) octet-list))

                   (t (error 'encode-error :seq seq :pos pos))))
    (nreverse octet-list)))
