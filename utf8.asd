(defpackage "UTF-8-SYSTEM"
  (:use "CL" "ASDF"))

(in-package "UTF-8-SYSTEM")

(defsystem "UTF-8"
  :serial t
  :components ((:file "defpackage")
               (:file "decode-sequence")
               (:file "encode-sequence")))
